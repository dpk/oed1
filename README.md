# The <cite>Oxford English Dictionary</cite> First Edition

Programs for, and notes on the CD-ROM edition of the first edition of
the <cite>Oxford English Dictionary<cite>.

The goal of this project is to make <cite>OED1</cite> accessible in
digital form in the 21st century.

Note that until such time as the copyright on the <cite>OED1</cite> is
determined with certainty to have expired, the actual data file will
not be distributed here.

## `CDROM.FIL`

SHA-1:

```
68d98d5c9880d4ee35daff05d540ba32eaed702f  CDROM.FIL
```

This file seemingly consists of search index data, possibly some image
and other tabular data from the body of the dictionary, then the
entire text of the dictionary in a kind of concise SGML format where
some tags are replaced by high- and low-ASCII binary codes, probably
to save space. There is seemingly no compression other than this, and
no copy protection.

The dictionary text starts at or just after 0x11D73800 and runs to
0x267BD2FA (the end of the file, modulo some null padding).

The reason I think there may be image/table data stored elsewhere in
the file is that the following table from the very first entry in
<cite>OED1</cite>:

![Pronunciations of the letter A in words](img/a-table.png)

is represented by the byte sequence `00 00 D6 07 09 10 FF 07 04 0F`.
This could be a pointer to an image or something stored elsewhere. The
two tables immediately after it are each represented by just `0F`.
None of the text in these tables appears in a simple grep search, thus
I think it’s more likely that they’re images. The tables for the
pronunciation of the letter are is represented by one byte `0F`; the table for the
historical inflection for the pronoun ‘I’ is represented by six bytes
of `0F`. (Possibly, one ‘table’ each for the ‘Old English’, ‘Middle
English’, and ‘Modern English’ headings plus one table each for the
inflectional tables below those headings.)

Every 0x1000 bytes starting at 0x800 (i.e. continuing at 0x1800,
0x2800 etc.) there appears to be 12 bytes of ‘junk’ of completely
unknown purpose. This possibly indicates that my initial extraciton starts
0x200 bytes ‘late’. They do appear to be counted in the lengths of the
formatting codes. Also appears to be at every 0xA000?

## `OED1.data`

Our name for the slightly minified SGML, pulled out of the raw CD-ROM
data directly:

```shell
dd if=CDROM.FIL of=OED1.data skip=146151 count=169108 bs=2048
```

## Formatting codes and their apparent meaning

Formatting codes appear to generally be one byte, followed by another
byte (uint8) indicating the length in bytes of the text they affect.
It looks (based on the sequence at 0x12A) that if the byte is 0xFF,
then two more bytes follow giving the length as a uint16 (big endian),
apparently plus two for the ‘real’ length. So at 0x12A we have 0x04
for ‘sense’ or ‘definition’ or similar, then 0xFF to indicate a longer
span of text, then 0x0176 (= 374), then 372 bytes of definition text
before a quotation paragraph.

There also seem to be some ‘extended codes’ which start with one or
two nulls, then give their length (including this code and the length)
as a uint16. See 0xEF80227, where we have `00 00 56`, then a length of
`00 F7` (= 247), then 242 bytes of apparent etymological note (though
there is also a `<note>` tag which seems somewhat redundant). Indeed,
just before that we have `00 00 43`, then a length of `00 47` (= 71),
then 68 bytes of the main etymology.

### Low ASCII

| byte | meaning                                                  |
|------|----------------------------------------------------------|
| 0x03 | sense number                                             |
| 0x04 | definition                                               |
| 0x10 | etymology or headword section? also variant form list 🤔 |

### Printable ASCII

| byte              | meaning                                          |
|-------------------|--------------------------------------------------|
| 0x20 (space)      | quotation author. also pronunciation?            |
| 0x21 (!)          | surrounds the ‘tr.’ when citing translated texts |
| 0x30 (digit zero) | quotation work title                             |
| 0x31 (digit one)  | quotation location citation                      |
| 0x40 (@)          | beginning of quotation text                      |

### High ASCII

HINT: Set Hex Fiend to ‘Latin-US (DOS)’! High ASCII appears to be Code
Page 437!

| byte | meaning                                  |
|------|------------------------------------------|
| 0xAE | beginning of character name («)          |
| 0xAF | end of character name (»)                |
| 0xC4 | em dash (box drawings single horizontal) |
| 0xF9 | *single* stress dot                      |

Check that « and » aren’t used for actual guillemets anywhere.

## Extended codes

The actual pattern here appears to be something like: one null byte,
12 bits of counter, 4 bits of type, then a uint16 length field.
Examples listed below.

| code       | meaning                                                                                           |
|------------|---------------------------------------------------------------------------------------------------|
| `00 xx x1` | entry heading (headword, pos, pronunciation)                                                      |
| `00 xx x2` | variant form list (0x1A18A84)                                                                     |
| `00 xx x3` | etymology (0x1A18BB3)                                                                             |
| `00 xx x4` | sense or definition text                                                                          |
| `00 xx x5` | quotation                                                                                         |
| `00 xx x6` | paragraph break?                                                                                  |
| `00 xx x7` | appears at entry headers to maybe introduce a machine-readable index of senses, yet to be decoded |

## SGML tags

These are sometimes quite different from the ones later used for OED2.

| tag name | meaning                                                                                                                                                                                                |
|----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `cf`     | Effectively italic: although the intention appears to have been to represent the *c*itation *f*orm of a word (in an etymology etc.), it is also used somewhat indiscriminately for other italicization |
| `hwlem`  | Headword lemma                                                                                                                                                                                         |
| `i`      | Italic                                                                                                                                                                                                 |
| `lab`    | Label                                                                                                                                                                                                  |
| `note`   | Small-type note                                                                                                                                                                                        |
| `snum`   | Sense number (in cross references only – actual sense numbers are given by magic codes)                                                                                                                |
| `su`     | Superscript                                                                                                                                                                                            |
| `vd`     | Variant form date (single-digit system)                                                                                                                                                                |
| `vf`     | Variant form                                                                                                                                                                                           |
| `xlem`   | Cross-referenced lemma                                                                                                                                                                                 |

## Character names

| name | character (description)                       |
|------|-----------------------------------------------|
| sdd  | double stress dot (used for secondary stress) |


<!-- Local Variables: -->
<!-- truncate-lines: t -->
<!-- End: -->
